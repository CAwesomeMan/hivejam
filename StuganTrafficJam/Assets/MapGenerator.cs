﻿using UnityEngine;
using System.Collections;

public struct MapPosition {
    public int x;
    public int y;
}

public class MapGenerator : MonoBehaviour {

    public GameObject CrossingPrefab;
    public GameObject ParkingPrefab;
    public GameObject RoadPrefab;
    public GameObject BuildingPrefab;

    public float TileSize;
    private int amountOfRows;

    public static MapGenerator instance;

    private MapStructure structure;
    private ParkingStates parkingStates;

    // Use this for initialization
    void Start() {
        structure = new MapStructure();
        structure.FillWithData();
        amountOfRows = (int)Mathf.Sqrt(structure.DataMatrix.Length);
        parkingStates = new ParkingStates();
        parkingStates.FillUpWithDataFromStructure(structure);
        DrawOutMap();

        instance = this;
    }
    void DrawOutMap() {
        for(int rowIndex = 0; rowIndex < amountOfRows; ++rowIndex) {
            for(int colIndex = 0; colIndex < amountOfRows; ++colIndex) {
                MapPosition pos = new MapPosition(){x = rowIndex, y = colIndex};
                DrawOutTile(pos);
            }
        }
    }

    void DrawOutTile(MapPosition pos) {
        RoadType typeOfRoad = structure.DataMatrix[pos.x, pos.y];
        GameObject road = RoadFromType(typeOfRoad, pos.y);
        PositionRoad(road, pos);
    }

    void PositionRoad(GameObject road, MapPosition pos) {
        Vector3 newPos = WorldCoordinatesForMapPosition(pos);
        road.transform.position = newPos;
    }

    GameObject RoadFromType(RoadType roadType, int rowIndex) {
        GameObject newRoad;
        switch(roadType) {
        case RoadType.Crossing:
            {
                newRoad = Instantiate(CrossingPrefab) as GameObject;
                break;
            }
        case RoadType.Building:
            {
                newRoad = Instantiate(BuildingPrefab) as GameObject;
                break;
            }
        case RoadType.ParkingSpot:
            {
                newRoad = Instantiate(ParkingPrefab) as GameObject;
                break;
            }
        case RoadType.Road:
            {
                newRoad = Instantiate(RoadPrefab) as GameObject;
                break;
            }
        default:
            Debug.LogError("NOT SUPPOSED TO BE HERE. UNHANDLED CASE IN ROADTYPE");
            newRoad = new GameObject();
            break;
        }

        bool rowIsUneven = rowIndex % 2 == 1;
        if((roadType == RoadType.ParkingSpot || roadType == RoadType.Road) && rowIsUneven) {
            //rotate road
            Vector3 currentRotation = newRoad.transform.rotation.eulerAngles;
            Quaternion newRot = Quaternion.Euler(currentRotation.x, currentRotation.y, currentRotation.z + 90.0f);
            newRoad.transform.rotation = newRot;
            Debug.Log("want to rotate!");
        }

        return newRoad;

    }

    public bool CanCarDriveHere(MapPosition pos) {
        if(pos.x < 0 || pos.x + 1 > amountOfRows || pos.y < 0 || pos.y + 1 > amountOfRows) {
            return false;
        }
        RoadType typeOfRoad = structure.DataMatrix[pos.x, pos.y];
        return typeOfRoad == RoadType.Crossing || typeOfRoad == RoadType.Road || typeOfRoad == RoadType.ParkingSpot;
    }

    public bool CanCarParkHere(MapPosition pos, DrivingDirection direction) {
        if(pos.x < 0 || pos.x + 1 > amountOfRows || pos.y < 0 || pos.y + 1 > amountOfRows) {
            return false;
        }
        bool CanPark = parkingStates.CanCarParkHere(pos, direction);
        return CanPark;
        
    }

    public void ParkCar(MapPosition pos, DrivingDirection direction) {
        parkingStates.ParkAtSpot(pos, direction);
    }

    public Vector3 WorldCoordinatesForMapPosition(MapPosition pos) {
        return new Vector3(TileSize * pos.x, -TileSize * pos.y, 0);
    }

    public Vector3 WorldCoordinatesForMapPosition(MapPosition pos, bool hasParked, int[] drivingDirection) {
        int verticalDirection = drivingDirection[1];
        int horizontallDirection = drivingDirection[0];
        Vector3 regularPos = WorldCoordinatesForMapPosition(pos);
        Vector3 offSet = Vector3.zero;
        float parkingOffset = 0.003f;
        float laneOffset = 0.001f;
        if(verticalDirection > 0) {
            offSet += new Vector3(-laneOffset, 0, 0);
            if(hasParked) {
                offSet += new Vector3(-parkingOffset, 0, 0);
            }
        }
        else if(verticalDirection < 0) {
            offSet += new Vector3(laneOffset, 0, 0);
            if(hasParked) {
                offSet += new Vector3(parkingOffset, 0, 0);
            }
        }
        else if(horizontallDirection > 0) {
            offSet += new Vector3(0, -laneOffset, 0);
            if(hasParked) {
                offSet += new Vector3(0, -parkingOffset, 0);
            }
        }
        else if(horizontallDirection < 0) {
            offSet += new Vector3(0, laneOffset, 0);
            if(hasParked) {
                offSet += new Vector3(0, parkingOffset, 0);
            }
        }
        else {
            Debug.LogError("car is standing still. WHY?!");
        }
        Vector3 newPos = regularPos + offSet;
        return newPos;
    }




}
