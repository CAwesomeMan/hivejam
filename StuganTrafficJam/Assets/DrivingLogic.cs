﻿using UnityEngine;
using System.Collections;
using System;

public enum DrivingDirection {
    Left = 0,
    Right,
    Up,
    Down,
}

public class DrivingLogic : MonoBehaviour {


    public float TimeBetweenDrivings = 1.0f;
    private float NextTimeToMove;
    public bool ShouldUseEPICAI = false;

    private MapPosition previousPosition;
    private MapPosition currentPosition;

    public int[] initPos;
    public int[] initPrevPos;

    public float RandomiseOffset = 0.1f;

    bool HasParked;


    // Use this for initialization
    void Start() {
        NextTimeToMove = Time.time + TimeBetweenDrivings;

        previousPosition = new MapPosition(){x = initPrevPos[0],y = initPrevPos[1]};
        currentPosition = new MapPosition(){x = initPos[0],y = initPos[1]};
    }
	
    // Update is called once per frame
    void Update() {
        if(NextTimeToMove < Time.time && !HasParked) {
            HasParked = MapGenerator.instance.CanCarParkHere(currentPosition, currentDrivingDirection());
            Vector3 nextPos = Vector3.zero;
            if(HasParked) {
                nextPos = MapGenerator.instance.WorldCoordinatesForMapPosition(currentPosition, true, currentDirection());
                MapGenerator.instance.ParkCar(currentPosition, currentDrivingDirection());
            }
            else {
                nextPos = NewPosition();
                Rotate();
            }
            float newWindow = TimeBetweenDrivings + UnityEngine.Random.Range(-RandomiseOffset, RandomiseOffset);
            NextTimeToMove += newWindow;
            StartCoroutine(DriveToNextPosition(nextPos, newWindow));
        }
    }
    void Rotate() {
        int verticalDirection = currentPosition.y - previousPosition.y;
        int horizontallDirection = currentPosition.x - previousPosition.x;
        if(verticalDirection > 0) {
//            Debug.Log("going down!");
            transform.rotation = Quaternion.Euler(0, 0, 270.0f);
        }
        else if(verticalDirection < 0) {
//            Debug.Log("going up!");
            transform.rotation = Quaternion.Euler(0, 0, 90.0f);
        }
        else if(horizontallDirection < 0) {
//            Debug.Log("going left!");
            transform.rotation = Quaternion.Euler(0, 0, 180.0f);
        }
        else if(horizontallDirection > 0) {
//            Debug.Log("going right!");
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else {
            Debug.LogError("car is standing still. WHY?!");
        }
    }

    Vector3 NewPosition() {
        if(ShouldUseEPICAI) {
            //use EPIC AI
            return Vector3.zero;
        }
        else {
            bool HasGoodTarget = false;
            MapPosition newPos;
            int iterations = 0;
            do {
                newPos = IsFreeParkingNear();
                if(newPos.x == -1 && newPos.y == -1) {
                    newPos = GetNewSuggestedPosition();
                }
                else {
                    Debug.Log("Found GOOD PARKING");
                }
                HasGoodTarget = MapGenerator.instance.CanCarDriveHere(newPos);
            } while(!HasGoodTarget && ++iterations < 100);

            if(iterations > 20) {
                Debug.Log("So many iterations");
                newPos = previousPosition;
            }
            previousPosition = currentPosition;
            currentPosition = newPos;

            Vector3 newPosition = MapGenerator.instance.WorldCoordinatesForMapPosition(newPos, false, currentDirection());
            return newPosition;
        }
    }

    IEnumerator DriveToNextPosition(Vector3 newPos, float animationDuration) {
        Vector3 startPos = transform.position;
        float progress = 0;
        while(progress < 1.0f) {
            progress = Mathf.Clamp01(progress + Time.deltaTime / animationDuration);
            transform.position = Vector3.Lerp(startPos, newPos, progress);
            yield return null;
        }

    }

    MapPosition GetNewSuggestedPosition() {
        int verticalDirection = currentPosition.y - previousPosition.y;
        int horizontallDirection = currentPosition.x - previousPosition.x;
        int newDirectionIndex = UnityEngine.Random.Range(0, 4);
        int[] newDirection = {horizontallDirection,verticalDirection};
        switch(newDirectionIndex) {
        case 0:
            {
                //turn left
                newDirection = RotatedDirection((double)newDirection[0], (double)newDirection[1], -Mathf.PI / 2.0f);
                break;
            }
        case 1:
        case 2:
                //Do nothin
            break;
        case 3:
            {
                newDirection = RotatedDirection((double)newDirection[0], (double)newDirection[1], Mathf.PI / 2.0f);
                break;
            }
        default:
            Debug.Log("Unhandled cases");
            break;
        }
            
        MapPosition newPos = new MapPosition(){x = currentPosition.x + newDirection[0],y = currentPosition.y + newDirection[1]};
        return newPos;
    }

    int[] RotatedDirection(double x, double y, double radians) {
        int[] newDir = new int[2];
        double newXDir = x * Math.Cos(radians) - y * Math.Sin(radians);
        double newYDir = x * Math.Sin(radians) + y * Math.Cos(radians);
        newDir[0] = Mathf.RoundToInt((float)newXDir);
        newDir[1] = Mathf.RoundToInt((float)newYDir);
        return newDir;
    }

    int[] currentDirection() {
        int verticalDirection = currentPosition.y - previousPosition.y;
        int horizontallDirection = currentPosition.x - previousPosition.x;
        return new int[] {
            horizontallDirection,
            verticalDirection
        };
    }

    DrivingDirection currentDrivingDirection() {
        return DirectionFromArray(currentDirection());
    }

    public static DrivingDirection DirectionFromArray(int[] direction) {
        DrivingDirection newDirection = DrivingDirection.Down;
        int verticalDirection = direction[1];
        int horizontallDirection = direction[0];
        if(verticalDirection > 0) {
            //            Debug.Log("going down!");
            newDirection = DrivingDirection.Down;
                
        }
        else if(verticalDirection < 0) {
            newDirection = DrivingDirection.Up;
            
        }
        else if(horizontallDirection < 0) {
            newDirection = DrivingDirection.Left;
            
        }
        else if(horizontallDirection > 0) {
            newDirection = DrivingDirection.Right;
        }
        return newDirection;
    }

    MapPosition IsFreeParkingNear() { 
        MapPosition tempParkingPos;
        
        int verticalDirection = currentPosition.y - previousPosition.y;
        int horizontalDirection = currentPosition.x - previousPosition.x;
        int[] newDirection = {horizontalDirection,verticalDirection};
        //Start checking left
        int[] leftDirection = RotatedDirection((double)newDirection[0], (double)newDirection[1], -Mathf.PI / 2.0f);
        tempParkingPos = new MapPosition(){x = currentPosition.x + leftDirection[0],y = currentPosition.y + leftDirection[1]};
        DrivingDirection leftDrivingDirection = DirectionFromArray(leftDirection);
        if(MapGenerator.instance.CanCarParkHere(tempParkingPos, leftDrivingDirection)) {
            return tempParkingPos;
        }
        //Check Straight
        //straight direction
        newDirection[0] = horizontalDirection; // I am unsure if the RotetedDirection change these values so I am resetign them
        newDirection[1] = verticalDirection;
        
        tempParkingPos = new MapPosition(){x = currentPosition.x + newDirection[0],y = currentPosition.y + newDirection[1]};
        DrivingDirection straightDirection = DirectionFromArray(newDirection);
        if(MapGenerator.instance.CanCarParkHere(tempParkingPos, straightDirection)) {
            return tempParkingPos;
        }
        //Check right
        int[] rightDirection = RotatedDirection((double)newDirection[0], (double)newDirection[1], Mathf.PI / 2.0f);
        tempParkingPos = new MapPosition(){x = currentPosition.x + rightDirection[0],y = currentPosition.y + rightDirection[1]};
        DrivingDirection rightDrivingDirection = DirectionFromArray(rightDirection);
        if(MapGenerator.instance.CanCarParkHere(tempParkingPos, rightDrivingDirection)) {
            return tempParkingPos;
        }
        //else 

        return new MapPosition(){x= -1, y=-1};
    }
    
}
