﻿using System.Collections;
using UnityEngine;


public enum ParkingState {
    BothFree = 0,
    ATaken,
    BTaken,
    BothTaken,
    NoParking,
}


public class ParkingStates {

    private ParkingState[,] parkingStates;

    public ParkingStates() {

    }
	

    public void FillUpWithDataFromStructure(MapStructure structure) {
        int amountOfRows = (int)Mathf.Sqrt(structure.DataMatrix.Length);
        parkingStates = new ParkingState[amountOfRows, amountOfRows];
        for(int rowIndex = 0; rowIndex < amountOfRows; ++rowIndex) {
            for(int colIndex = 0; colIndex < amountOfRows; ++colIndex) {
                if(structure.DataMatrix[rowIndex, colIndex] == RoadType.ParkingSpot) {
                    parkingStates[rowIndex, colIndex] = ParkingState.BothFree;
                }
                else {
                    parkingStates[rowIndex, colIndex] = ParkingState.NoParking;
                }
            }
        }

    }

    public void ParkAtSpot(MapPosition pos, DrivingDirection direction) {
        ParkingState currentStateForSpot = parkingStates[pos.x, pos.y];
        ParkingState newState = ParkingState.BothTaken;
        switch(direction) {
        case DrivingDirection.Left:
        case DrivingDirection.Up:
            {
                if(currentStateForSpot == ParkingState.ATaken) {
                    newState = ParkingState.BothTaken;
                }
                else {
                    newState = ParkingState.BTaken;
                }
                break;
            }
        case DrivingDirection.Down:
        case DrivingDirection.Right:
            {
                if(currentStateForSpot == ParkingState.BTaken) {
                    newState = ParkingState.BothTaken;
                }
                else {
                    newState = ParkingState.ATaken;
                }
                break;
            }
        }
        parkingStates[pos.x, pos.y] = newState;
        
    }
    
    public bool CanCarParkHere(MapPosition MapPosition, DrivingDirection direction) {
        if(parkingStates[MapPosition.x, MapPosition.y] == ParkingState.NoParking) {
            return false;
        }

        ParkingState currentStateForSpot = parkingStates[MapPosition.x, MapPosition.y];

        bool OkToPark = false;
        switch(direction) {
        case DrivingDirection.Left:
        case DrivingDirection.Up:
            {
                OkToPark = currentStateForSpot == ParkingState.ATaken ||
                    currentStateForSpot == ParkingState.BothFree;
                break;
            }
        case DrivingDirection.Down:
        case DrivingDirection.Right:
            {
                OkToPark = currentStateForSpot == ParkingState.BTaken ||
                    currentStateForSpot == ParkingState.BothFree;
                break;
            }
        }
        return OkToPark;

    }

    public MapPosition GiveFreeParkingSpot() {
        return new MapPosition(){x=0,y=0};
    }
}
