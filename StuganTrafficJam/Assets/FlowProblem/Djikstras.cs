﻿using System;
using UnityEngine;
using System.Collections.Generic;

class Graph {
    Dictionary<string, Dictionary<string, int>> vertices = new Dictionary<string, Dictionary<string, int>>();
		
    public void add_vertex(string name, Dictionary<string, int> edges) {
        vertices[name] = edges;
    }
		
    public List<string> shortest_path(string start, string finish) {
        var previous = new Dictionary<string, string>();
        var distances = new Dictionary<string, int>();
        var nodes = new List<string>();
			
        List<string> path = null;
			
        foreach(var vertex in vertices) {
            if(vertex.Key == start) {
                distances[vertex.Key] = 0;
            }
            else {
                distances[vertex.Key] = int.MaxValue;
            }
				
            nodes.Add(vertex.Key);
        }
			
        while(nodes.Count != 0) {
            nodes.Sort((x, y) => distances[x] - distances[y]);
				
            var smallest = nodes[0];
            nodes.Remove(smallest);
				
            if(smallest == finish) {
                path = new List<string>();
                while(previous.ContainsKey(smallest)) {
                    path.Add(smallest);
                    smallest = previous[smallest];
                }
					
                break;
            }
				
            if(distances[smallest] == int.MaxValue) {
                break;
            }
				
            foreach(var neighbor in vertices[smallest]) {
                var alt = distances[smallest] + neighbor.Value;
                if(alt < distances[neighbor.Key]) {
                    distances[neighbor.Key] = alt;
                    previous[neighbor.Key] = smallest;
                }
            }
        }
			
        return path;
    }
}
	
class PathSolver {
    public PathSolver() {
        Graph g = new Graph();
        g.add_vertex("1-1", new Dictionary<string, int>() {{"2-2", 7}, {"3-3", 8}});
        g.add_vertex("2-2", new Dictionary<string, int>() {{"1-1", 7}, {"6-6", 2}});
        g.add_vertex("3-3", new Dictionary<string, int>() {{"1-1", 8}, {"6-6", 6}, {"7-7", 4}});
        g.add_vertex("4-4", new Dictionary<string, int>() {{"6-6", 8}});
        g.add_vertex("5-5", new Dictionary<string, int>() {{"8-8", 1}});
        g.add_vertex("6-6", new Dictionary<string, int>() {{"2-2", 2}, {"3-3", 6}, {"4-4", 8}, {"7-7", 9}, {"8-8", 3}});
        g.add_vertex("7-7", new Dictionary<string, int>() {{"3-3", 4}, {"6-6", 9}});
        g.add_vertex("8-8", new Dictionary<string, int>() {{"5-5", 1}, {"6-6", 3}});
        
        g.shortest_path("1-1", "8-8").ForEach(x => Debug.Log(x));
    }
}
