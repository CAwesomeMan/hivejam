﻿using System.Collections;


public enum RoadType {
    Crossing = 0,
    ParkingSpot,
    Road,
    Building,
}

public class MapStructure {

    private RoadType[,] dataMatrix;
    public RoadType[,] DataMatrix {
        get {
            return dataMatrix;
        }
    }

    public MapStructure() {

    }

    public void FillWithData() {

        RoadType[,] roadMatrix =

        { {
            RoadType.Road,
            RoadType.Building,
                RoadType.ParkingSpot,
                RoadType.Building,
                RoadType.ParkingSpot,
            RoadType.Building,
                RoadType.ParkingSpot,
                RoadType.Building,
                RoadType.ParkingSpot,
                RoadType.Building,
        }, {
            RoadType.Crossing,
            RoadType.ParkingSpot,
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
            RoadType.ParkingSpot,
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
                RoadType.ParkingSpot,
            }, {
                RoadType.ParkingSpot,
                RoadType.Building,
            RoadType.Road,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building
        }, {
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
            RoadType.Road
        }, {
                RoadType.ParkingSpot,
                RoadType.Building,
            RoadType.ParkingSpot,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building
        }, {
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
            RoadType.ParkingSpot,
            RoadType.Crossing,
                RoadType.ParkingSpot,
                RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
                RoadType.ParkingSpot,
            }, {
            RoadType.Road,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building,
                RoadType.ParkingSpot,
                RoadType.Building,
            RoadType.Road,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building
        }, {
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
                RoadType.ParkingSpot,
                RoadType.Crossing,
            RoadType.ParkingSpot,
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
            RoadType.Road
        }, {
            RoadType.Road,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building,
            RoadType.Road,
            RoadType.Building,
                RoadType.ParkingSpot,
                RoadType.Building,
            RoadType.Road,
            RoadType.Building
        }, {
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
                RoadType.ParkingSpot,
                RoadType.Crossing,
            RoadType.Road,
            RoadType.Crossing,
            RoadType.Road
        }
        };
//        {{2,3,2,3,2,3,2,3,2,3},
//            {0,2,0,2,0,2,0,2,0,2},
//            {2,3,2,3,2,3,2,3,2,3},
//            {0,2,0,2,0,2,0,2,0,2},
//            {2,3,2,3,2,3,2,3,2,3},
//            {0,2,0,2,0,2,0,2,0,2},
//            {2,3,2,3,2,3,2,3,2,3},
//            {0,2,0,2,0,2,0,2,0,2},
//            {2,3,2,3,2,3,2,3,2,3},
//            {0,2,0,2,0,2,0,2,0,2},
//            {2,3,2,3,2,3,2,3,2,3}
//        };
        dataMatrix = roadMatrix;

    }

}
